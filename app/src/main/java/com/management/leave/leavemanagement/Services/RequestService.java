package com.management.leave.leavemanagement.Services;

import android.net.Uri;
import android.util.Log;

import com.management.leave.leavemanagement.Extensions._URL;
import com.management.leave.leavemanagement.Models.UserRequest;

import java.net.URL;

import static com.management.leave.leavemanagement.Helpers.Global.BASIC_URL;

/**
 * Created by mirza on 7/13/18.
 */

public class RequestService {
    private static final String TAG = RequestService.class.getSimpleName();

    public static URL requestURL(UserRequest.Type type) {
        boolean isProcesed = true;
        if (type==UserRequest.Type.Processing)
            isProcesed = false;

        Uri uri = Uri.parse(BASIC_URL).buildUpon().
                appendPath("Requests").
                appendQueryParameter("id", "8").
                appendQueryParameter("isProcessed", String.valueOf(isProcesed)).
                build();

        Log.v(TAG, "Built url" + uri.toString());
        return new _URL().createUrl(uri);
    }

    public static URL requestTypesURL() {
        Uri uri = Uri.parse(BASIC_URL).buildUpon().
                appendPath("Requests").
                appendPath("Types").
                build();

        Log.v(TAG, "Built url" + uri.toString());
        return new _URL().createUrl(uri);
    }

    public static URL addRequestURL() {
        Uri uri = Uri.parse(BASIC_URL).buildUpon().
                appendPath("Requests").
                appendPath("Add").
                build();

        Log.v(TAG, "Built url" + uri.toString());
        return new _URL().createUrl(uri);
    }

    public static URL cancelRequestURL(int id) {
        Uri uri = Uri.parse(BASIC_URL).buildUpon().
                appendPath("Requests").
                appendPath("Cancel").
                appendQueryParameter("Id",String.valueOf(id)).
                build();

        Log.v(TAG, "Built url" + uri.toString());
        return new _URL().createUrl(uri);
    }
}
