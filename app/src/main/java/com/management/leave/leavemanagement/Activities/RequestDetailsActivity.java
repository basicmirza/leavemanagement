package com.management.leave.leavemanagement.Activities;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.management.leave.leavemanagement.Helpers.DateConverter;
import com.management.leave.leavemanagement.Helpers.Response;
import com.management.leave.leavemanagement.Helpers.ResponseHandler;
import com.management.leave.leavemanagement.Managers.RequestManager;
import com.management.leave.leavemanagement.Models.UserRequest;
import com.management.leave.leavemanagement.R;

import java.util.Date;

public class RequestDetailsActivity extends BaseActivity {
    private UserRequest request;
    private TextView mType;
    private TextView mState;
    private TextView mDaysAbsense;
    private TextView mStartDate;
    private TextView mEndDate;
    private TextView mComment;
    private TextView mCreatedAt;
    private Button mCancelRequest;
    private ImageView mStateIcon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_details);
        setUI();
        if (getIntent().hasExtra("request")) {
            request = (UserRequest) getIntent().getSerializableExtra("request");
            setDetails();
        }
    }

 private void setDetails(){
     mType.setText(request.TypeOfRequestText);
     mState.setText(request.LeaveState);
     mDaysAbsense.setText(request.DaysAbsence.toString());
     mStartDate.setText(DateConverter.to_dd_mm_yyyy(request.StartDate));
     mEndDate.setText(DateConverter.to_dd_mm_yyyy(request.EndDate));
     mComment.setText(request.Comment);
     mCreatedAt.setText(DateConverter.to_dd_mm_yyyy(request.RequestDate));

     if (request.StateId == UserRequest.Type.Processing.getValue()) {
         mCancelRequest.setVisibility(View.VISIBLE);
     } else {
         mCancelRequest.setVisibility(View.GONE);
     }

     int state = request.StateId;
     if (state == UserRequest.Type.Approved.getValue()) {
         mState.setTextColor(getResources().getColor(R.color.aprovedColor));
         mStateIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_done_black_24dp));
         mStateIcon.setColorFilter(ContextCompat.getColor(this, R.color.aprovedColor), android.graphics.PorterDuff.Mode.SRC_IN);
     } else if (state == UserRequest.Type.NotAproved.getValue()) {
         mState.setTextColor(getResources().getColor(R.color.notAprovedColor));
         mStateIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_block_black_24dp));
         mStateIcon.setColorFilter(ContextCompat.getColor(this, R.color.notAprovedColor), android.graphics.PorterDuff.Mode.SRC_IN);
     } else if (state == UserRequest.Type.Processing.getValue()) {
         mState.setTextColor(getResources().getColor(R.color.processingColor));
         mStateIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_access_time_black_24dp));
         mStateIcon.setColorFilter(ContextCompat.getColor(this, R.color.processingColor), android.graphics.PorterDuff.Mode.SRC_IN);
     } else if (state == UserRequest.Type.Canceled.getValue()) {
         mState.setTextColor(getResources().getColor(R.color.canceledColor));
         mStateIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_close_black_24dp));
         mStateIcon.setColorFilter(ContextCompat.getColor(this, R.color.canceledColor), android.graphics.PorterDuff.Mode.SRC_IN);
     }

 }
 private void setUI() {
     mType = findViewById(R.id.type);
     mStartDate = findViewById(R.id.startDate);
     mState = findViewById(R.id.state);
     mEndDate = findViewById(R.id.endDate);
     mCreatedAt = findViewById(R.id.created);
     mComment = findViewById(R.id.comment);
     mDaysAbsense = findViewById(R.id.daysAbsence);
     mCancelRequest = findViewById(R.id.cancel_request);
     mCancelRequest.setVisibility(View.GONE);
     mStateIcon = findViewById(R.id.stateIcon);

 }

 public void clickCancelRequest(final View view) {
     view.setEnabled(false);
     RequestManager.getInstance().cancelRequest(request.Id, new ResponseHandler<Response<UserRequest>>() {
         @Override
         public void handle(Response<UserRequest> response) {
             view.setEnabled(true);
             if (response.success) {
                 request = response.data;
                 setDetails();
             }

         }
     });
 }
}
