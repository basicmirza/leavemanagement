package com.management.leave.leavemanagement.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.management.leave.leavemanagement.Activities.RequestDetailsActivity;
import com.management.leave.leavemanagement.Helpers.DateConverter;
import com.management.leave.leavemanagement.Models.UserRequest;
import com.management.leave.leavemanagement.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by mirza on 7/12/18.
 */

public class RequestsAdapter extends RecyclerView.Adapter<RequestsAdapter.RequestAdapterViewHolder> {

    private Context context;
    private List<UserRequest> mRequests;
    RequestAdapterOnClickHandler clickHandler;
    public RequestsAdapter(RequestAdapterOnClickHandler clickHandler){
        this.clickHandler=clickHandler;
        mRequests = new ArrayList<>();

    }

    public void setData(List<UserRequest> data) {
        mRequests = data;
        notifyDataSetChanged();
    }

    public interface RequestAdapterOnClickHandler{
        void OnClick(UserRequest request);
    }


    @NonNull
    @Override
    public RequestAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context=parent.getContext();
        int layoutIdFromItemlist= R.layout.request_list_item;
        LayoutInflater inflater=LayoutInflater.from(context);
        View view=inflater.inflate(layoutIdFromItemlist,parent,false);
        return new RequestAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestAdapterViewHolder holder, int position) {
        UserRequest request = mRequests.get(position);
        holder.mType.setText(request.TypeOfRequestText);
        holder.mState.setText(request.LeaveState);
        holder.mStartDate.setText(DateConverter.to_dd_mm_yyyy(request.StartDate));
        holder.mEndDate.setText(DateConverter.to_dd_mm_yyyy(request.EndDate));


        int type = request.StateId;
        if (type == UserRequest.Type.Approved.getValue()) {
            holder.mState.setTextColor(context.getResources().getColor(R.color.aprovedColor));
            holder.mStateIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_done_black_24dp));
            holder.mStateIcon.setColorFilter(ContextCompat.getColor(context, R.color.aprovedColor), android.graphics.PorterDuff.Mode.SRC_IN);
        } else if (type == UserRequest.Type.NotAproved.getValue()) {
            holder.mState.setTextColor(context.getResources().getColor(R.color.notAprovedColor));
            holder.mStateIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_block_black_24dp));
            holder.mStateIcon.setColorFilter(ContextCompat.getColor(context, R.color.notAprovedColor), android.graphics.PorterDuff.Mode.SRC_IN);
        } else if (type == UserRequest.Type.Processing.getValue()) {
            holder.mState.setTextColor(context.getResources().getColor(R.color.processingColor));
            holder.mStateIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_access_time_black_24dp));
            holder.mStateIcon.setColorFilter(ContextCompat.getColor(context, R.color.processingColor), android.graphics.PorterDuff.Mode.SRC_IN);
        } else if (type == UserRequest.Type.Canceled.getValue()) {
            holder.mState.setTextColor(context.getResources().getColor(R.color.canceledColor));
            holder.mStateIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_close_black_24dp));
            holder.mStateIcon.setColorFilter(ContextCompat.getColor(context, R.color.canceledColor), android.graphics.PorterDuff.Mode.SRC_IN);
        }

    }

    @Override
    public int getItemCount() {
        return mRequests==null?0:mRequests.size();
    }

    public class RequestAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mType ;
        TextView mState ;
        TextView mStartDate ;
        TextView mEndDate ;
        ImageView mStateIcon ;


            public RequestAdapterViewHolder(View itemView) {
            super(itemView);
            mType = itemView.findViewById(R.id.type);
            mState = itemView.findViewById(R.id.state);
            mStartDate = itemView.findViewById(R.id.startDate);
            mEndDate = itemView.findViewById(R.id.endDate);
            mStateIcon = itemView.findViewById(R.id.stateIcon);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickHandler.OnClick(mRequests.get(getAdapterPosition()));
        }
    }

}
