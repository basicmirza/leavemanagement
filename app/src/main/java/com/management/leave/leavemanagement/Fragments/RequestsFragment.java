package com.management.leave.leavemanagement.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.management.leave.leavemanagement.Activities.RequestDetailsActivity;
import com.management.leave.leavemanagement.Adapters.RequestsAdapter;
import com.management.leave.leavemanagement.Helpers.Response;
import com.management.leave.leavemanagement.Helpers.ResponseHandler;
import com.management.leave.leavemanagement.Managers.RequestManager;
import com.management.leave.leavemanagement.Models.UserRequest;
import com.management.leave.leavemanagement.R;

import java.util.List;


public class RequestsFragment extends BaseFragment implements RequestsAdapter.RequestAdapterOnClickHandler {
    private SwipeRefreshLayout mSwipeRefreshRecycleView;
    private View view;
    private UserRequest.Type type;
    private RequestsAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private TextView mMessage;
    private TextView mMessageError;

    public RequestsFragment() {

    }

    public static RequestsFragment newInstance(UserRequest.Type type) {
        RequestsFragment fragment = new RequestsFragment();
        Bundle arg = new Bundle();
        arg.putSerializable("type", type);
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_requests,container,false);

        mRecyclerView = view.findViewById(R.id.recycleView);
        mSwipeRefreshRecycleView= view.findViewById(R.id.swipeRefresh);
        mMessage = view.findViewById(R.id.message);
        mMessageError = view.findViewById(R.id.message_error);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getRequests(type);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        int recyclerViewOrientation = LinearLayoutManager.VERTICAL;

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), recyclerViewOrientation, false);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new RequestsAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mSwipeRefreshRecycleView.setRefreshing(true);
        Bundle arg = getArguments();
        if (arg!=null && arg.containsKey("type")) {
            type = (UserRequest.Type) arg.getSerializable("type");
            getRequests(type);
        }


        mSwipeRefreshRecycleView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRequests(type);
            }
        });
    }

    private void getRequests(UserRequest.Type type) {
        RequestManager.getInstance().getRequests(type, new ResponseHandler<Response<List<UserRequest>>>() {
            @Override
            public void handle(Response<List<UserRequest>> response) {
                if (response.success) {
                    mMessageError.setVisibility(View.GONE);
                    if (response.data.size()>0) {
                        mMessage.setVisibility(View.GONE);
                    }
                    else {
                        mMessage.setVisibility(View.VISIBLE);
                    }
                } else {
                    mMessage.setVisibility(View.GONE);
                    mMessageError.setVisibility(View.VISIBLE);
                }
                mAdapter.setData(response.data);
                mSwipeRefreshRecycleView.setRefreshing(false);
            }
        });
    }

    @Override
    public void OnClick(UserRequest request) {

        Intent i = new Intent(this.getActivity().getBaseContext(), RequestDetailsActivity.class);
        i.putExtra("request", request);
        startActivity(i);
    }

}
