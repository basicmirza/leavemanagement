package com.management.leave.leavemanagement.Services;

import android.net.Uri;
import android.util.Log;

import com.management.leave.leavemanagement.Extensions._URL;

import java.net.URL;

import static com.management.leave.leavemanagement.Helpers.Global.BASIC_URL;

/**
 * Created by mirza on 7/15/18.
 */

public class HomeService {
    private static final String TAG = HomeService.class.getSimpleName();

    public static URL DetailsURL(int userId) {
        Uri uri = Uri.parse(BASIC_URL).buildUpon().
                appendPath("Home").
                appendQueryParameter("userId", String.valueOf(userId)).
                build();

        Log.v(TAG, "Built url" + uri.toString());
        return new _URL().createUrl(uri);
    }
}
