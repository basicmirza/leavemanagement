package com.management.leave.leavemanagement.Managers;


import android.os.AsyncTask;

import com.management.leave.leavemanagement.Helpers.Response;
import com.management.leave.leavemanagement.Helpers.ResponseHandler;
import com.management.leave.leavemanagement.Models.Session;
import com.management.leave.leavemanagement.Services.LoginService;
import com.management.leave.leavemanagement.Services.NetworkService;

import java.io.IOException;
import java.net.URL;

public class LoginManager {

    private static LoginManager instance;
    public static LoginManager getInstance() {
        if (instance == null) {
            instance = new LoginManager();
        }
        return instance;
    }

    public void loginUser(String username, String password, final ResponseHandler<Response<Session>> handler){

        URL url = LoginService.LoginURL(username,password);
        new AsyncTask<URL, Void, Response<Session>>() {

            @Override
            protected Response<Session> doInBackground(URL... params) {
                try {
                  return NetworkService.getObjectResponse(params[0],Session.class);
                } catch (IOException e) {
                    e.printStackTrace();
                    return new Response(null,false);
                }
            }

            @Override
            protected void onPostExecute(Response<Session> response) {
                super.onPostExecute(response);
                handler.handle(response);
            }
        }.execute(url);

    }
}
