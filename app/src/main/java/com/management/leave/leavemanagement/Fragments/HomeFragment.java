package com.management.leave.leavemanagement.Fragments;


import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.management.leave.leavemanagement.Activities.CreateLeaveRequestActivity;
import com.management.leave.leavemanagement.Helpers.Response;
import com.management.leave.leavemanagement.Helpers.ResponseHandler;
import com.management.leave.leavemanagement.Helpers.SessionSharedPreferences;
import com.management.leave.leavemanagement.Managers.HomeManager;
import com.management.leave.leavemanagement.Models.HomeUserDetails;
import com.management.leave.leavemanagement.Models.Session;

import com.management.leave.leavemanagement.R;



public class HomeFragment extends BaseFragment implements View.OnClickListener {
    private View view;
    private TextView mDaysUsed;
    private HomeUserDetails mDetails;
    private TextView mDaysLeft;
    private TextView mDaysInCompany;
    private FloatingActionButton mCreateRequestButton;

    public HomeFragment() {

    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view=inflater.inflate(R.layout.fragment_home,container,false);

        mCreateRequestButton = view.findViewById(R.id.createNewRequest);
        mDaysLeft = view.findViewById(R.id.daysLeft);
        mDaysUsed = view.findViewById(R.id.daysUsed);
        mDaysInCompany = view.findViewById(R.id.daysInCompany);
        mCreateRequestButton.setOnClickListener(this);
        Session user = SessionSharedPreferences.GetSignInUser(getActivity().getBaseContext());
        HomeManager.getInstance().getHomeDetails(user.Id, new ResponseHandler<Response<HomeUserDetails>>() {
            @Override
            public void handle(Response<HomeUserDetails> response) {
                if (response.success) {
                    mDetails = response.data;
                    setUI();
                }
            }
        });

        return view;
    }

    private void setUI() {
        if (mDetails!=null) {
            counterAnimation(mDaysLeft,mDetails.VacationDaysLeft);
            counterAnimation(mDaysUsed,mDetails.VacationDaysUsed);
            counterAnimation(mDaysInCompany, mDetails.DaysInCompany);
        }
    }

    public void counterAnimation(final TextView view, int count) {
        ValueAnimator animator = new ValueAnimator();
        animator.setObjectValues(0, count);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                view.setText(String.valueOf(animation.getAnimatedValue()));
            }
        });
        animator.setDuration(1300);
        animator.start();
    }

    @Override
    public void onClick(View view) {
       switch (view.getId()){
           case R.id.createNewRequest:
               Intent i = new Intent(getActivity().getBaseContext(), CreateLeaveRequestActivity.class);
               startActivity(i);
               break;
       }
    }
}
