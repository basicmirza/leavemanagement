package com.management.leave.leavemanagement.Services;

import android.net.Uri;
import android.util.Log;

import com.management.leave.leavemanagement.Extensions._URL;

import java.net.URL;

import static com.management.leave.leavemanagement.Helpers.Global.BASIC_URL;

/**
 * Created by mirza on 3/2/18.
 */

public class LoginService {

    private static final String TAG = LoginService.class.getSimpleName();

    public static URL LoginURL(String username, String password) {
        Uri uri = Uri.parse(BASIC_URL).buildUpon().
                appendPath("Login").
                appendQueryParameter("email", username).
                appendQueryParameter("password",password).
                build();

        Log.v(TAG, "Built url" + uri.toString());
        return new _URL().createUrl(uri);
    }
}
