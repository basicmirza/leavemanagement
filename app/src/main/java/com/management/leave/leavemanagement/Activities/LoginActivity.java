package com.management.leave.leavemanagement.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.management.leave.leavemanagement.Helpers.Response;
import com.management.leave.leavemanagement.Helpers.ResponseHandler;
import com.management.leave.leavemanagement.Helpers.SessionSharedPreferences;
import com.management.leave.leavemanagement.Managers.LoginManager;
import com.management.leave.leavemanagement.Models.Session;
import com.management.leave.leavemanagement.R;

public class LoginActivity extends AppCompatActivity {


    EditText mEmail;
    EditText mPassword;
    Button mSingInButton;
    TextView mMessage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setUI();
    }

    public void setUI(){
       mSingInButton=findViewById(R.id.signIn);
       mEmail=findViewById(R.id.email);
       mPassword=findViewById(R.id.password);
       mMessage = findViewById(R.id.message);

       mPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mMessage.setVisibility(View.INVISIBLE);
            }
        });
        mPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                mMessage.setVisibility(View.INVISIBLE);
            }
        });
    }

    public void clickSignInButton(View view) {
       disableForm();
        LoginManager.getInstance().loginUser(mEmail.getText().toString(), mPassword.getText().toString(), new ResponseHandler<Response<Session>>() {
            @Override
            public void handle(Response<Session> response) {
                enableForm();
                if (response.success && response.data!=null) {
                    SessionSharedPreferences.SaveSignInUser(response.data, getBaseContext());
                    Intent i = new Intent(getBaseContext(), FirstActivity.class);
                    startActivity(i);
                } else {
                    mMessage.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void disableForm(){
        mEmail.setEnabled(false);
        mPassword.setEnabled(false);
        mSingInButton.setEnabled(false);
    }

    public void enableForm(){
        mEmail.setEnabled(true);
        mPassword.setEnabled(true);
        mSingInButton.setEnabled(true);
    }

}
