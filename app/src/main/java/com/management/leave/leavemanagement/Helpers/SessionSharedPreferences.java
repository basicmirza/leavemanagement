package com.management.leave.leavemanagement.Helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.management.leave.leavemanagement.Models.Session;

public class SessionSharedPreferences {

        public static String USER_SHARED_PREFERENCE_KEY = "user_key";
        private static final String PREFS_NAME = "SharedPreferences";

        public static void SaveSignInUser(Session signInAccount, Context context) {
            String jsonUser = GsonConverter.ObjectToJson(signInAccount);
            SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(USER_SHARED_PREFERENCE_KEY, jsonUser);
            editor.commit();
        }

        public static Session GetSignInUser(Context context) {
            SharedPreferences sharedPreferences = PreferenceManager
                    .getDefaultSharedPreferences(context);
            String jsonUser = sharedPreferences.getString(USER_SHARED_PREFERENCE_KEY, "");
            if (jsonUser.length() == 0) {
                return null;
            }
            return GsonConverter.JsonToObject(jsonUser, Session.class);
        }

        public static void logout(Context context){
            SaveSignInUser(null,context);
        }
}

