package com.management.leave.leavemanagement.Activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;

import com.management.leave.leavemanagement.Helpers.DateConverter;
import com.management.leave.leavemanagement.Helpers.Response;
import com.management.leave.leavemanagement.Helpers.ResponseHandler;
import com.management.leave.leavemanagement.Helpers.SessionSharedPreferences;
import com.management.leave.leavemanagement.MainActivity;
import com.management.leave.leavemanagement.Managers.RequestManager;
import com.management.leave.leavemanagement.Models.RequestTypes;
import com.management.leave.leavemanagement.Models.Session;
import com.management.leave.leavemanagement.Models.UserRequest;
import com.management.leave.leavemanagement.R;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CreateLeaveRequestActivity extends AppCompatActivity {


    private Button mStartDateButton;
    private Date mStartDate;
    private Date mEndDate;
    private Button mEndDateButton;
    private Button mTypeOfRequest;
    private View mTypePickerlayout;
    private NumberPicker mTypePicker;
    private int mTypeValueIndex;
    private List<RequestTypes> mRequestTypes;
    private List<String> mRequestTypesStrings = new ArrayList<>();
    private Date mMinDateStart;
    private Date mMinDateEnd;
    private Button mSave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_leave_request);
        mStartDateButton = findViewById(R.id.startDate);
        mEndDateButton = findViewById(R.id.endDate);
        mTypeOfRequest = findViewById(R.id.typeOfRequest);
        mTypePickerlayout = findViewById(R.id.typePickerlayout);
        mTypePicker = findViewById(R.id.typePicker);
        mTypePickerlayout.setVisibility(View.GONE);
        mSave = findViewById(R.id.create);

        mTypeValueIndex = -1;
        mSave.setEnabled(isFormValid());

        final Calendar mCalendar = Calendar.getInstance();
        mMinDateStart = mCalendar.getTime();
        mCalendar.add(Calendar.DATE,+1);
        mMinDateEnd = mCalendar.getTime();

        getRequestTypes();
        mTypePicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i2) {
                mTypeValueIndex = i2;
            }
        });
    }

    public void getRequestTypes() {
        mTypeOfRequest.setEnabled(false);
        RequestManager.getInstance().getRequestTypes(new ResponseHandler<Response<List<RequestTypes>>>() {
            @Override
            public void handle(Response<List<RequestTypes>> response) {
                if (response.success) {
                    mTypeOfRequest.setEnabled(true);
                    mRequestTypes = response.data;
                    for (RequestTypes type:mRequestTypes) {
                        mRequestTypesStrings.add(type.Name);
                    }
                    mTypePicker.setMinValue(0);
                    mTypePicker.setMaxValue(mRequestTypes.size()-1);
                    mTypePicker.setDisplayedValues(mRequestTypesStrings.toArray(new String[0]));
                }
            }
        });
    }

    public void clickCreate(View view) throws ParseException {
        if (mStartDate.getTime() < mEndDate.getTime() && mTypeValueIndex!=-1) {
            disableForm();
            final UserRequest request = new UserRequest();

            request.StartDate = mStartDate;
            request.EndDate = mEndDate;
            request.UserId = SessionSharedPreferences.GetSignInUser(this).Id;
            request.TypeOfRequestId = mRequestTypes.get(mTypeValueIndex).Id;
            RequestManager.getInstance().addRequest(request, new ResponseHandler<Response<UserRequest>>() {
                @Override
                public void handle(Response<UserRequest> response) {
                    enableForm();
                    if (response.success) {
                        Intent i = new Intent(getBaseContext(), RequestDetailsActivity.class);
                        i.putExtra("request", response.data);
                        startActivity(i);
                        finish();
                    }
                }
            });
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Message");
            alertDialog.setMessage("Starting date must be smaller than end date!");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
    }

    public void clickDoneTypePicker(View view) {
        enableForm();
        mTypePickerlayout.setVisibility(View.GONE);
        if (mTypeValueIndex==-1) {
            mTypeValueIndex=0;
        }
        mTypeOfRequest.setText(mRequestTypesStrings.get(mTypeValueIndex));
        mSave.setEnabled(isFormValid());
    }

    public void disableForm() {
        mStartDateButton.setEnabled(false);
        mEndDateButton.setEnabled(false);
        mTypeOfRequest.setEnabled(false);
    }

    public void enableForm() {
        mStartDateButton.setEnabled(true);
        mEndDateButton.setEnabled(true);
        mTypeOfRequest.setEnabled(true);
    }

    public void openPicker(View view) {
        switch (view.getId()) {
            case R.id.startDate:
                openDatePicker((Button)view, mMinDateStart, mEndDate, mStartDate);
                break;
            case R.id.endDate:
                openDatePicker((Button)view, mStartDate!=null?mStartDate:mMinDateEnd, null, mEndDate);
                break;
            case R.id.typeOfRequest:
                openTypePicker();
                break;
        }
    }

    public void openTypePicker() {
        disableForm();
        mTypePickerlayout.setVisibility(View.VISIBLE);
    }

    public void openDatePicker(final Button button, Date minDate, Date maxDate, Date selectedDate) {
        final Calendar myCalendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "MM/dd/yy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

                button.setText(sdf.format(myCalendar.getTime()));
                if (button == mStartDateButton) {
                    mStartDate = myCalendar.getTime();
                } else if (button == mEndDateButton) {
                    mEndDate = myCalendar.getTime();
                }
                mSave.setEnabled(isFormValid());
            }

        };
        DatePickerDialog dialog;
        if (selectedDate!=null) {
            dialog = new DatePickerDialog(this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
        } else {
            dialog = new DatePickerDialog(this, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH));
        }
        dialog.getDatePicker().setMinDate(minDate.getTime());
        if (maxDate!=null) {
            dialog.getDatePicker().setMaxDate(maxDate.getTime());
        }

        dialog.show();
    }

    private Boolean isFormValid() {
        if (mStartDate!=null && mEndDate!=null && mTypeValueIndex!=-1) {
            return true;
        } else {
            return false;
        }
    }
}
