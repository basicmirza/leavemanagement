package com.management.leave.leavemanagement.Services;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.management.leave.leavemanagement.Helpers.GsonConverter;
import com.management.leave.leavemanagement.Helpers.Response;
import com.management.leave.leavemanagement.Helpers.ResponseHandler;


import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class NetworkService {

    private static final String TAG = NetworkService.class.getSimpleName();

    public static <T>  Response<T> getListResponse(URL url, Type type) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        int responseCode = urlConnection.getResponseCode();

        if (responseCode != 200) {
            return new Response(null, responseCode, false);
        } else {

            try {
                InputStream in = urlConnection.getInputStream();
                Scanner scanner = new Scanner(in);
                scanner.useDelimiter("\\A");

                boolean hasInput = scanner.hasNext();
                if (hasInput) {
                    T data = (T) GsonConverter.JsonToListArray(scanner.next(), type);
                    return new Response(data, responseCode, true);
                } else {
                    return new Response(null, true);
                }
            } finally {
                urlConnection.disconnect();
            }
        }
    }

    public static <T>  Response<T> getObjectResponse(URL url, Class<T> tClass) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        int responseCode = urlConnection.getResponseCode();

        if (responseCode != 200) {
            return new Response(null, responseCode, false);
        } else {

            try {
                InputStream in = urlConnection.getInputStream();
                Scanner scanner = new Scanner(in);
                scanner.useDelimiter("\\A");

                boolean hasInput = scanner.hasNext();
                if (hasInput) {
                    T data = GsonConverter.JsonToObject (scanner.next(), tClass);
                    return new Response(data, responseCode, true);
                } else {
                    return new Response(null, true);
                }
            } finally {
                urlConnection.disconnect();
            }
        }
    }


    public static <T> Response<T> postResponse(URL url, String jsonParameters, Class<T> tClass) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        try {
            urlConnection.setDoOutput(true);
            urlConnection.setChunkedStreamingMode(0);
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");

            Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));

            if (!jsonParameters.equals("")) {
                writer.write(jsonParameters);
                writer.close();
            }

            int responseCode = urlConnection.getResponseCode();

            if (responseCode != 200) {
                return new Response(null, responseCode, false);

            } else {
                InputStream in = urlConnection.getInputStream();
                Scanner scanner = new Scanner(in);
                scanner.useDelimiter("\\A");
                boolean hasInput = scanner.hasNext();
                if (hasInput) {
                    T responseData = GsonConverter.JsonToObject(scanner.next(), tClass);
                    return new Response(responseData, true);
                } else {
                    return new Response(null, true);
                }
            }
        } finally {
            urlConnection.disconnect();
        }
    }

    public static Bitmap getBitmapImageFromUri(Uri uri) throws Exception{
         try {
             URL url = new URL(uri.toString());
             InputStream in = url.openStream();
             ByteArrayOutputStream stream = new ByteArrayOutputStream();
             Bitmap image= BitmapFactory.decodeStream(in);
             image.compress(Bitmap.CompressFormat.PNG,100,stream);
             return image;

         } catch (Exception e) {
             /* TODO log error */
         }
         return null;
    }

    public static Response<byte[]> getImageDataFromUri(Uri uri) throws Exception{
        try {
            URL url = new URL(uri.toString());
            InputStream in = url.openStream();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Bitmap image= BitmapFactory.decodeStream(in);
            image.compress(Bitmap.CompressFormat.PNG,100,stream);
            return new Response<byte[]>(stream.toByteArray(),true);

        } catch (Exception e) {
            return new Response<byte[]>(null,false);
        }
    }
}
