package com.management.leave.leavemanagement.Helpers;

/**
 * Created by mirza on 3/2/18.
 */

public class Response<T> {
  public T data;
  public Boolean success = false;
  public Error error;
  public int statusCode = 0;

  public Response(T data, Boolean success, Error error, int statusCode) {
      this.data=data;
      this.success=success;
      this.error = error;
      this.statusCode =statusCode;
  }

  public Response(Error error, int statusCode) {

      this.error = error;
      this.statusCode =statusCode;
  }

  public Response(T data, int statusCode, Boolean success) {
      this.data = data;
      this.statusCode = statusCode;
      this.success = success;
  }

  public Response(T data, Boolean success, Error error) {
      this.data=data;
      this.success=success;
      this.error = error;
  }
  public Response(T data, Boolean success) {
      this.data=data;
      this.success=success;
      this.error = null;
  }
}
