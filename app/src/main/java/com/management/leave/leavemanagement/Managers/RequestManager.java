package com.management.leave.leavemanagement.Managers;

import android.os.AsyncTask;

import com.google.gson.reflect.TypeToken;
import com.management.leave.leavemanagement.Helpers.GsonConverter;
import com.management.leave.leavemanagement.Helpers.Response;
import com.management.leave.leavemanagement.Helpers.ResponseHandler;
import com.management.leave.leavemanagement.Models.RequestTypes;
import com.management.leave.leavemanagement.Models.Session;
import com.management.leave.leavemanagement.Models.UserRequest;
import com.management.leave.leavemanagement.Services.LoginService;
import com.management.leave.leavemanagement.Services.NetworkService;
import com.management.leave.leavemanagement.Services.RequestService;

import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mirza on 7/13/18.
 */

public class RequestManager {

    private static RequestManager instance;
    public static RequestManager getInstance() {
        if (instance == null) {
            instance = new RequestManager();
        }
        return instance;
    }

    Type listTypeRequest = new TypeToken<ArrayList<UserRequest>>() {
    }.getType();
    Type listTypeRequestTypes = new TypeToken<ArrayList<RequestTypes>>() {
    }.getType();

    public void getRequests(UserRequest.Type type ,final ResponseHandler<Response<List<UserRequest>>> handler){

        URL url = RequestService.requestURL(type);
        new AsyncTask<URL, Void, Response<List<UserRequest>>>() {

            @Override
            protected Response<List<UserRequest>> doInBackground(URL... params) {
                try {
                    return NetworkService.getListResponse(params[0],listTypeRequest);
                } catch (IOException e) {
                    e.printStackTrace();
                    return new Response(null,false);
                }
            }

            @Override
            protected void onPostExecute(Response<List<UserRequest>> response) {
                super.onPostExecute(response);
                handler.handle(response);
            }
        }.execute(url);

    }

    public void getRequestTypes(final ResponseHandler<Response<List<RequestTypes>>> handler){

        URL url = RequestService.requestTypesURL();
        new AsyncTask<URL, Void, Response<List<RequestTypes>>>() {

            @Override
            protected Response<List<RequestTypes>> doInBackground(URL... params) {
                try {
                    return NetworkService.getListResponse(params[0],listTypeRequestTypes);
                } catch (IOException e) {
                    e.printStackTrace();
                    return new Response(null,false);
                }
            }

            @Override
            protected void onPostExecute(Response<List<RequestTypes>> response) {
                super.onPostExecute(response);
                handler.handle(response);
            }
        }.execute(url);

    }

    public void addRequest(final UserRequest request, final ResponseHandler<Response<UserRequest>> handler){

        URL url = RequestService.addRequestURL();
        new AsyncTask<URL, Void, Response<UserRequest>>() {

            @Override
            protected Response<UserRequest> doInBackground(URL... params) {
                try {
                    return NetworkService.postResponse(params[0], GsonConverter.ObjectToJson(request), UserRequest.class);
                } catch (IOException e) {
                    e.printStackTrace();
                    return new Response(null,false);
                }
            }

            @Override
            protected void onPostExecute(Response<UserRequest> response) {
                super.onPostExecute(response);
                handler.handle(response);
            }
        }.execute(url);

    }

    public void cancelRequest(final int Id, final ResponseHandler<Response<UserRequest>> handler){

        URL url = RequestService.cancelRequestURL(Id);
        new AsyncTask<URL, Void, Response<UserRequest>>() {

            @Override
            protected Response<UserRequest> doInBackground(URL... params) {
                try {
                    return NetworkService.postResponse(params[0], "", UserRequest.class);
                } catch (IOException e) {
                    e.printStackTrace();
                    return new Response(null,false);
                }
            }

            @Override
            protected void onPostExecute(Response<UserRequest> response) {
                super.onPostExecute(response);
                handler.handle(response);
            }
        }.execute(url);

    }
}
