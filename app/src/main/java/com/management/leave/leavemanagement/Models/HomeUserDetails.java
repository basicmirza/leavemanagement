package com.management.leave.leavemanagement.Models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by mirza on 7/15/18.
 */

public class HomeUserDetails implements Serializable {

    public int DaysInCompany;
    public int VacationDaysUsed;
    public int VacationDaysLeft;

}
