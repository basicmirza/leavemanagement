package com.management.leave.leavemanagement.Helpers;

import java.io.Serializable;

/**
 * Created by mirza on 3/2/18.
 */

public interface ResponseHandler <Response> extends Serializable {
    void handle(Response t);
}
