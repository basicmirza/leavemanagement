package com.management.leave.leavemanagement.Models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Developer on 7/12/18.
 */

public class UserRequest  implements Serializable {
    public int Id;
    public int UserId;
    public Date StartDate;
    public Date EndDate;
    public int StateId;
    public String LeaveState;
    public String UserName;
    public Date RequestDate;
    public Integer DaysAbsence;
    public int TypeOfRequestId;
    public String TypeOfRequestText;
    public String Comment;


    public enum Type {
        Approved(1),Canceled(2),NotAproved(3), Processing(4), Processed(5);

        private final int value;

        private Type(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

}
