package com.management.leave.leavemanagement.Managers;

import android.os.AsyncTask;

import com.management.leave.leavemanagement.Helpers.Response;
import com.management.leave.leavemanagement.Helpers.ResponseHandler;
import com.management.leave.leavemanagement.Helpers.SessionSharedPreferences;
import com.management.leave.leavemanagement.Models.HomeUserDetails;
import com.management.leave.leavemanagement.Models.Session;
import com.management.leave.leavemanagement.Services.HomeService;
import com.management.leave.leavemanagement.Services.LoginService;
import com.management.leave.leavemanagement.Services.NetworkService;

import java.io.IOException;
import java.net.URL;


public class HomeManager {
    private static HomeManager instance;
    public static HomeManager getInstance() {
        if (instance == null) {
            instance = new HomeManager();
        }
        return instance;
    }

    public void getHomeDetails(int userId,final ResponseHandler<Response<HomeUserDetails>> handler){

        URL url = HomeService.DetailsURL(userId);
        new AsyncTask<URL, Void, Response<HomeUserDetails>>() {

            @Override
            protected Response<HomeUserDetails> doInBackground(URL... params) {
                try {
                    return NetworkService.getObjectResponse(params[0],HomeUserDetails.class);
                } catch (IOException e) {
                    e.printStackTrace();
                    return new Response(null,false);
                }
            }

            @Override
            protected void onPostExecute(Response<HomeUserDetails> response) {
                super.onPostExecute(response);
                handler.handle(response);
            }
        }.execute(url);

    }
}
