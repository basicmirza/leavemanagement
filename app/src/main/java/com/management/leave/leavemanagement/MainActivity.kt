package com.management.leave.leavemanagement

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.management.leave.leavemanagement.Activities.FirstActivity
import com.management.leave.leavemanagement.Fragments.HomeFragment
import com.management.leave.leavemanagement.Fragments.RequestsFragment
import com.management.leave.leavemanagement.Helpers.SessionSharedPreferences
import com.management.leave.leavemanagement.Models.UserRequest
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    var selectedFragment: Fragment? = null
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                selectedFragment = HomeFragment.newInstance();
                supportFragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_processing -> {
                selectedFragment = RequestsFragment.newInstance(UserRequest.Type.Processing);
                supportFragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_processed -> {
                selectedFragment = RequestsFragment.newInstance(UserRequest.Type.Processed);
                supportFragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        selectedFragment = HomeFragment.newInstance();
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.logout_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.logout -> {
                SessionSharedPreferences.logout(this);
                val intent = Intent(this, FirstActivity::class.java)
                startActivity(intent)
            }
        }
        return true
    }

    override fun onResume() {
        super.onResume()
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();
    }


}
