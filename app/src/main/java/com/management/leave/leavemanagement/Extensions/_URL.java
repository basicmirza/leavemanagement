package com.management.leave.leavemanagement.Extensions;

import android.net.Uri;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;

public class _URL {
    public URL createUrl(Uri uri)
    {
        java.net.URL url=null;
        try {
            url=new java.net.URL(uri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }
}

